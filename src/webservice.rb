#!/usr/bin/env ruby
# coding: utf-8

require "sinatra"
require "sinatra/flash"
require "json"
require "dotenv"

Dotenv.load

require_relative "database"

set :static, true
set :public_folder, File.dirname(__FILE__) + "/../public"

enable :sessions

get "/" do
    @services = Page.all
    erb :index
end

get "/json" do
    services = Page.all

    result = services.map do |service|
        {
            name: service.name,
            description: service.description,
            url: "#{service.protocol}://#{service.url}#{service.path}",
            slug: service.slug,
            response: {
                online: service.status.is_up?,
                code: service.status.status_code,
                message: service.status.status_message,
                time: service.status.response_time
            }
        }
    end

    content_type "application/json"
    JSON::dump(result)
end

get "/service/:slug.json" do |slug|
    service = Page.first(slug: slug)

    result = {
        name: service.name,
        description: service.description,
        url: "#{service.protocol}://#{service.url}#{service.path}",
        slug: service.slug,
        response: {
            online: service.status.is_up?,
            code: service.status.status_code,
            message: service.status.status_message,
            time: service.status.response_time
        }
    }

    content_type "application/json"
    JSON::dump(result)
end

get "/service/:slug" do |slug|
    @service = Page.first(slug: slug)
    erb :service
end
