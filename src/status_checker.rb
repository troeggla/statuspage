require 'net/http'

class StatusChecker
    attr_writer :follow_redirects

    def initialize protocol, host, path
        @protocol, @host, @path = protocol, host, path
        @follow_redirects = true
    end

    def http_auth username, password
        @username, @password = username, password
    end

    def is_up?
        begin
            @response ||= query_server
            @response.kind_of? Net::HTTPOK
        rescue
            false
        end
    end

    def status_code
        begin
            @response ||= query_server
            @response.code
        rescue
            nil
        end
    end

    def status_message
        begin
            @response ||= query_server
            @response.message
        rescue
            "No response"
        end
    end

    def response_time
        begin
            @response ||= query_server
            "%.0f" % [@end * 1000]
        rescue
            nil
        end
    end

private
    def query_server
        start = Time.now
        result = nil

        uri = URI("#{@protocol}://#{@host}#{@path}")
        http = Net::HTTP.new uri.host, uri.port

        req = Net::HTTP::Get.new uri

        unless @username.nil? or @password.nil?
            req.basic_auth @username, @password
        end

        if @protocol == "https"
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end

        result = http.request req

        if result.code.to_i.between?(300, 399) and @follow_redirects
            @protocol, @host, @path = parse_url result['Location']
            result = query_server
        end

        @end = Time.now - start
        result
    end

    def parse_url url
        matches = %r{^([a-z]+)://([a-z.-]+)/?(.*)$}.match url

        unless matches.length == 4
            throw "Malformed URL"
        else
            protocol, host, path = matches[1..3]
            path = '/' if path == ""

            [protocol, host, path]
        end
    end
end
