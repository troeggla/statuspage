#!/usr/bin/env ruby

require 'data_mapper'

DataMapper.setup(:default, ENV['DATABASE_URL'])

require_relative "models/page"

DataMapper.finalize
DataMapper.auto_upgrade!
