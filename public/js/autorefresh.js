$(document).ready(function () {
    var timeout = setTimeout(function () {
        location.reload();
    }, 60000);

    $('#autorefresh').click(function () {
        if ($(this).is(':checked')) {
            timeout = setTimeout(function () {
                location.reload();
            }, 60000);
        } else {
            clearTimeout(timeout);
        }
    });
});
